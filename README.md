## Docs:
General - https://learnxinyminutes.com/docs/git/

Branching - http://learngitbranching.js.org/

Git flow - https://www.atlassian.com/git/tutorials/comparing-workflows

## Exercises:
https://try.github.io/levels/1/challenges/1

http://gitexercises.fracz.com/exercise/master


## Utils:

Git ignore generator - https://www.gitignore.io/api/osx,python,pycharm,django,
```
function gi() { curl -L -s https://www.gitignore.io/api/$@ ;}
```

## Useful aliases
```
[alias]
	glog = log --date-order --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit
	l = log --date-order --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit
	ls = log --date-order --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit --all
	up = rebase
    sl = stash list
    sa = stash apply
    ss = stash save
    r = reset
    r1 = reset HEAD^
    r2 = reset HEAD^^
    rh = reset --hard
    rh1 = reset HEAD^ --hard
    rh2 = reset HEAD^^ --hard
    cp = cherry-pick
    st = status
    cl = clone
    ci = commit
    co = checkout
    br = branch 
    diff = diff --word-diff
    dc = diff --cached
    la = "!git config -l | grep alias | cut -c 7-"
```